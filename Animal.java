public class Animal {
    public String _animalName;
    public boolean _isCarnivore;
    public double _topSpeed;

    public Animal(String animalName, Boolean isCarnivore, double topSpeed) {
        _animalName = animalName;
        _isCarnivore = isCarnivore;
        _topSpeed = topSpeed;
    }

    public boolean canAnimalCatchGazelle() {
        return (_isCarnivore && _topSpeed >= 75);
    }

    public void displayAnimalInfo() {
        //Print all fields 
        System.out.println("Animal name: " + _animalName);

        if (_isCarnivore)
            System.out.println("Animal diet: Carnivore");
        else if (!_isCarnivore)
            System.out.println("Animal diet: Herbivore");

        System.out.println("Animal top speed: " + _topSpeed + "km/h");
    }
}