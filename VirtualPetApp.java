import java.util.Scanner;

public class VirtualPetApp {
    public static void main(String[] args) {
        System.out.print("\033[H\033[2J"); //clear the console
        System.out.flush();  

        Scanner reader = new Scanner(System.in);

        Animal[] felines = new Animal[4];
        
        //For testing purposes :
        /*felines[0] = new Animal("Cheetah", true, 110);
        felines[1] = new Animal("Tiger", true, 65);
        felines[2] = new Animal("Lion", true, 74);
        felines[3] = new Animal("Jaguar", true, 80);
        */

        boolean isCarnivore;

        for (int i = 0; i < felines.length; i++) {
            System.out.print("\nEnter animal name: ");
            String name = reader.nextLine();

            System.out.print("\nIs animal carnivore? (Y/N): ");
            String diet = reader.nextLine().toLowerCase();

            if (diet.equals("y"))
                isCarnivore = true;
            else 
                isCarnivore = false;

            System.out.print("\nEnter animal top speed: ");
            double topSpeed = reader.nextDouble();
            reader.nextLine();
            
            //Create animal at each index
            felines[i] = new Animal(name, isCarnivore, topSpeed);

            System.out.print("\033[H\033[2J");  
            System.out.flush();  
        }
        
        //Print animals to make sure everything looks good
        //printAnimalArray(felines);

        //Step 14: Call 2 instance methods on first animal 
        if (felines[0].canAnimalCatchGazelle())
            System.out.println("\nAnimal #1 can catch a gazelle");
        else if (!felines[0].canAnimalCatchGazelle())
            System.out.println("\nAnimal #1 can't catch a gazelle");
        
        System.out.println("\nAnimal information:\n");
        felines[0].displayAnimalInfo();

        reader.close();
    }

    //I made this method just to make sure the data was stored correctly
    public static void printAnimalArray(Animal[] a){
        for(int i = 0; i < a.length; i++) {
            a[i].displayAnimalInfo();
            System.out.println();
        }
    }
}
